import React, { Component } from "react";
import { onPress, TouchableNativeFeedback, Image } from "react-native";
import {
  Container,
  FormLogin,
  ContainerSection,
  Button,
  Title,
  IconButton
} from "./components";
import { connect } from "react-redux";
import { Row, Left } from "native-base";
import Icon from "react-native-vector-icons/FontAwesome";

class Login extends Component {
  render() {
    const {
      headerContainer,
      headerLogo,
      blackColor,
      rowRegister,
      redColor,
      forget,
      textColorSmall,
      iconPos
    } = styles;
    return (
      <Container>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/headerLogo.png")}
            style={headerLogo}
          />

          <Title isheadLine iscenter style={blackColor}>
            login
          </Title>
        </ContainerSection>

        <ContainerSection>
          <Container style={{ position: "relative" }}>
            <FormLogin placeholder={"Email"} keyboardType={"email-address"} />
            <Icon style={iconPos} name={"envelope"} />
          </Container>
          <Container style={{ position: "relative" }}>
            <FormLogin placeholder={"Password"} secureTextEntry></FormLogin>
            <Icon style={iconPos} name={"eye-slash"} />
          </Container>

          <Title isforget style={forget}>Forget Password ?</Title>

        </ContainerSection>

        <ContainerSection>
          <Button title={"Login"} />
        </ContainerSection>

        <Title iscenter style={textColorSmall}>or</Title>

        <ContainerSection>
          <IconButton
            iconName={"envelope"}
            iconBgColor={"#AC3D32"}
            title={"Login dengan Gmail"}
          />

          <IconButton
            iconName={"facebook"}
            iconBgColor={"#395185"}
            title={"Login dengan Facebook"}
          />
        </ContainerSection>
        <ContainerSection style={rowRegister}>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("Register")}
          >
            <Title iscenter style={redColor}>
              Register{" "}
            </Title>
          </TouchableNativeFeedback>
          <Title iscenter style={textColorSmall} >untuk buat akun</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Login);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 63,
    color: "#424242"
  },
  rowRegister: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  redColor: {
    color: "red"
  },
  forget: {
    color: "#BDBDBD"
  },
  textColorSmall: {
    color: "#828282"
  },
  iconPos: {
    position: "absolute",
    marginLeft: 335,
    marginTop: 25
  }
};
