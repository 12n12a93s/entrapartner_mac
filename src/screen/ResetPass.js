import React, { Component } from "react";
import { onPress, Image, TouchableNativeFeedback } from "react-native";
import {
  Container,
  ContainerSection,
  Button,
  Title,
  FormLogin,
  ArrowBack
} from "./components";
import { connect } from "react-redux";

class ResetPass extends Component {
  render() {
    const {
      headerContainer,
      headerLogo,
      blackColor,
      rowRegister,
      redColor,
      smallText
    } = styles;
    return (
      <Container>
        <ContainerSection>
          <ArrowBack
            imgSrc={require("../assets/arrow.png")} />
        </ContainerSection>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/headerLogo.png")}
            style={headerLogo}
          />
          <Title iscenter style={blackColor}>
            Lupa Password
          </Title>
          <Title iscenter style={smallText}>
            Password baru akan kami kirimkan ke Email mu
          </Title>
          <Title style={smallText}>Silahkan masukkan Email aktif</Title>
        </ContainerSection>

        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>

        <ContainerSection>
          <FormLogin placeholder={"Email"} />
        </ContainerSection>

        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>

        <ContainerSection>
          <Button title={"Kirim"} />
        </ContainerSection>

        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>
        <ContainerSection></ContainerSection>

        <ContainerSection style={rowRegister}>
          <TouchableNativeFeedback
            onPress={() => this.props.navigation.navigate("Register")}
          >
            <Title iscenter style={redColor}>
              Register{" "}
            </Title>
          </TouchableNativeFeedback>
          <Title iscenter>untuk buat akun</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(ResetPass);

const styles = {
  headerContainer: {
    marginTop: 50,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 37,
    color: "#424242"
  },
  rowRegister: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center"
  },
  redColor: {
    color: "red"
  },
  smallText: {
    color: "#828282",
    fontSize: 10
  }
};
