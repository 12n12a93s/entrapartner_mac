import React, { Component } from "react";
import { Text, View, TouchableOpacity, Image } from "react-native";
import {
  Container,
  FormLogin,
  ContainerSection,
  Button,
  Title,
  IconButton,
  placeholder,
  keyboardType,
  secureTextEntery
} from "./components";
import { connect } from "react-redux";
import NavigationService from "../service/NavigationService";

import Icon from "react-native-vector-icons/FontAwesome";

class Register extends Component {
  render() {
    const {
      headerContainer,
      headerLogo,
      blackColor,
      rowRegister,
      redColor,
      green,
      iconPos,
      textColorSmall,
      textColor
    } = styles;
    const nama = this.props.navigation.getParam('nama', 'v')
    return (
      <Container>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/headerLogo.png")}
            style={headerLogo}
          />

          <Title isheadLine iscenter style={blackColor}>
            Register
          </Title>
        </ContainerSection>

        <ContainerSection>
          <Container style={{ position: "relative" }}>
            <FormLogin placeholder={"Email"} keyboardType={"email-address"} />
            <Icon style={iconPos} name={"envelope"} />
          </Container>
          <Container style={{ position: "relative" }}>
            <FormLogin placeholder={"Password"} secureTextEntry></FormLogin>
            <Icon style={iconPos} name={"eye-slash"} />
          </Container>
          <Container style={{ position: "relative" }}>
            <FormLogin placeholder={"Masukkan kembali"} secureTextEntry></FormLogin>
            <Icon style={iconPos} name={"eye-slash"} />
          </Container>
        </ContainerSection>

        <ContainerSection >
          <Container style={rowRegister}>
            <Title iscenter style={textColorSmall}>By registering, I agree with </Title>
            <Title iscenter style={green}>
              Term and Condition
          </Title>
          </Container>
        </ContainerSection >

        <ContainerSection style={rowRegister}>
          <Title iscenter style={textColorSmall}>and </Title>
          <Title iscenter style={green}>
            Privacy Police
          </Title>
          <Title iscenter style={textColorSmall}> of Entra.</Title>
        </ContainerSection >

        <ContainerSection>
          <Button title={"Register"} />
        </ContainerSection>

        <Title iscenter>or</Title>

        <ContainerSection>
          <IconButton
            iconName={"envelope"}
            iconBgColor={"#AC3D32"}
            title={"Signup dengan Gmail"}
          />

          <IconButton
            iconName={"facebook"}
            iconBgColor={"#395185"}
            title={"Signup dengan Facebook"}
          />
        </ContainerSection>
        <ContainerSection style={rowRegister}>
          <Title iscenter style={redColor}>
            Login{" "}
          </Title>
          <Title iscenter style={textColor}>untuk menjelajah aplikasi</Title>
        </ContainerSection>
      </Container >
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Register);

const styles = {
  headerContainer: {
    marginTop: 50,
    marginBottom: 0,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 63
  },
  rowRegister: {
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
    marginTop: -5
  },
  redColor: {
    color: "red"
  },
  green: {
    color: "#A9CF46",
    fontSize: 10
  },
  iconPos: {
    position: "absolute",
    marginLeft: 335,
    marginTop: 25
  },
  textColorSmall: {
    color: "#828282",
    fontSize: 10
  },
  textColor: {
    color: "#828282"
  }
};
