import React, { Component } from "react";
import { onPress, Image } from "react-native";
import {
    Container,
    ContainerSection,
    Button,
    Title,
    ArrowBack,
    RadioBtn
} from "./components";
import { connect } from "react-redux";

class ChooseBisnis extends Component {
    render() {
        const { rowTop, right, notifContainer, imgPos, titlePos } = styles;
        return (
            <Container>
                <ContainerSection style={rowTop}>
                    <ArrowBack
                        imgSrc={require("../assets/arrow.png")} />
                    <Title style={right}>Skip</Title>
                </ContainerSection>
                <ContainerSection>
                    <Title isheadline2 iscenter>Pilih Bisnismu</Title>
                    <RadioBtn
                        selected={true}
                        title={"Tempat Wisata / Wahana"}
                        subtitle={"Usaha wisata berupa wahana atau kunjungan wisata lain."} />
                    <RadioBtn
                        selected={false}
                        title={"Penginapan"}
                        subtitle={"Penginapan dapat berupa hotel, bnb, homestay, apartemen, lodge, resort, dll."} />
                    <RadioBtn
                        selected={false}
                        title={"Transportasi"}
                        subtitle={"Transportasi yang disewakan seperti mobil, bus, minibus, motor, dll."} />
                    <RadioBtn
                        selected={false}
                        title={"Souvenir"}
                        subtitle={"Usaha oleh-oleh baik berupa makanan, kerajinan tangan, dll."} />
                </ContainerSection>
                <ContainerSection style={notifContainer}>
                    <Image
                        source={require("../assets/notif.png")}
                        style={imgPos}
                    />
                    <Title style={titlePos}>Nanti kamu bisa menambahkan bisnis {'\n'} lain di menu "Bisnisku".</Title>
                </ContainerSection>
                <ContainerSection>
                    <Button
                        title={"Selanjutnya"} />
                </ContainerSection>
            </Container>
        )
    }
}
const mapStateProps = state => {
    return {};
};
export default connect(mapStateProps, {})(ChooseBisnis);

const styles = {
    rowTop: {
        flexDirection: 'row',
        position: "relative"
    },
    right: {
        paddingLeft: 320,
        marginTop: 20,
        fontSize: 14,
        color: "#828282",
        position: "absolute"
    },
    notifContainer: {
        position: "relative",
        marginTop: 20
    },
    titlePos: {
        position: "absolute",
        marginLeft: "16%",
        color: "#FFC923"
    },
    imgPos: {
        marginLeft: "5%",
        marginTop: 10
    }
}