import React, { Component } from "react";
import { onPress, Image } from "react-native";
import {
  Container,
  ContainerSection,
  Button,
  Title
} from "./components";
import { connect } from "react-redux";

class Welcome extends Component {
  render() {
    const { headerContainer, headerLogo,
      welcomeText,
      smallColor, right } = styles;
    return (
      <Container>
        <Title style={right}>Skip</Title>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/welcomeHome.png")}
            style={headerLogo}
          />
        </ContainerSection>

        <ContainerSection>
          <Title iscenter style={welcomeText}>Selamat Datang{'\n'} </Title>
          <Title iscenter style={smallColor}>Selamat bergabung dengan ENTRA{'\n'}
            Mulai sekarang bangun bisnis {'\n'}
            pariwisata sendiri </Title>
        </ContainerSection>

        <ContainerSection>
          <Button
            title={"Mulai Sekarang"} />
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Welcome);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 300,
    resizeMode: "contain"
  },
  right: {
    paddingLeft: 320,
    paddingTop: 10,
    fontSize: 14,
    color: "#828282"
  },
  welcomeText: {
    marginTop: 0,
    fontSize: 18,
    color: "#5c5c5c",
    fontWeight: '600'

  },
  smallColor: {
    marginTop: 0,
    fontSize: 16,
    color: "#828282",
    fontWeight: '300'
  }
};
