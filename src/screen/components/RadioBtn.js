import React from "react";
import { ContainerSection } from "./ContainerSection";
import { Content, ListItem, Radio, Text, Right, Left } from 'native-base';
import { Title } from "./Title";

export const RadioBtn = ({ title, selected, subtitle }) => {
    const { contain, rightItem } = styles;
    return (
        <Content>
            <ListItem>
                <Left style={rightItem}>
                    <Radio
                        color={"#BDBDBD"}
                        selectedColor={"#FFC923"}
                        selected={selected}
                    />
                </Left>
                <Right >
                    <Left>
                        <Title isheadline3>{title}</Title>
                        <Title isdescription>{subtitle}</Title>

                    </Left>
                </Right>
            </ListItem>
        </Content>

    );
};

const styles = {
    contain: {
        marginTop: 10,
    },
    rightItem: {
        marginRight: -900,
    }
};