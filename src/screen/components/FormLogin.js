import React from "react";
import { Form, Input, Item } from "native-base";

export const FormLogin = ({ placeholder, secureTextEntry, keyboardType }) => {
  const { inputStyle } = styles;
  return (
    <Form>
      <Item stackedLabel >
        <Input
          placeholder={placeholder}
          style={inputStyle}
          keyboardType={keyboardType}
          autoCapitalize={"none"}
          secureTextEntry={secureTextEntry}
        />
      </Item>
    </Form>

  );
};

const styles = {
  inputStyle: {
    fontSize: 14,
    color: "#000",
    paddingRight: 70
  }
};
