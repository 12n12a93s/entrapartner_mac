import React from "react";
import Icon from "react-native-vector-icons/FontAwesome";
import { Container } from "./Container";

export const IconButton = ({ title, iconName, iconBgColor }) => {
  const { textContainer, contain } = styles;
  return (
    <Container style={contain}>
      <Icon.Button
        name={iconName}
        backgroundColor={iconBgColor}
        onPress={this.loginWithFacebook}
        style={textContainer}
      >
        {title}
      </Icon.Button>
    </Container>
  );
};

const styles = {
  textContainer: {
    height: 48,
    alignItems: "center",
    justifyContent: "center"
  },
  contain: {
    paddingTop: 10
  }
};
