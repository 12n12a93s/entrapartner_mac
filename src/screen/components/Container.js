import React, { Fragment } from "react";
import { ScrollView, View, StatusBar, SafeAreaView } from "react-native";

export const Container = ({
  transcluent = false,
  StatusBarColor = "#fff",
  isTransparent = false,
  isCenter = false,
  contentContainerStyle,
  children,
  style
}) => {
  const { flexOne, scrollViewContainer } = styles;
  return (
    <Fragment>
      <StatusBar
        translucent={transcluent}
        backgroundColor={isTransparent ? "transparent" : StatusBarColor}
        barStyle={"dark-content"}
      />
      <SafeAreaView style={flexOne}>
        <ScrollView
          contentContainerStyle={[scrollViewContainer, contentContainerStyle]}
        >
          <View style={[flexOne, style]}>{children}</View>
        </ScrollView>
      </SafeAreaView>
    </Fragment>
  );
};

const styles = {
  flexOne: {
    flex: 1
  },
  scrollViewContainer: {
    flexGrow: 1
  }
};
