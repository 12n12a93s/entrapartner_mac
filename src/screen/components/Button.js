import React from "react";
import { TouchableNativeFeedback } from "react-native-gesture-handler";
import { Title } from "./Title";
import { View } from "react-native";

export const Button = ({ title, onPress, containerButton, textButton }) => {
  const { textContainer, colorWhite } = styles;
  return (
    <TouchableNativeFeedback onPress={onPress}>
      <View style={[textContainer, containerButton]}>
        <Title style={[colorWhite, textButton]}>{title}</Title>
      </View>
    </TouchableNativeFeedback>
  );
};

const styles = {
  textContainer: {
    height:48,
    backgroundColor: "#A9CF46",
    borderRadius: 5,
    alignItems: "center",
    justifyContent: "center",
    paddingVertical: 10
  },
  colorWhite: {
    color: "#fff"
  }
};
