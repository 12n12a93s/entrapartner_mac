import React from "react";
import { ContainerSection } from "./ContainerSection";
import { Image } from "react-native";

export const ArrowBack = ({ imgSrc }) => {
    const { arrow } = styles;
    return (
        <ContainerSection>
            <Image style={arrow}
                source={imgSrc} />
        </ContainerSection>

    );
};

const styles = {
    arrow: {
        marginTop: 10,
    }
};
