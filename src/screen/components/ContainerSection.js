import React from "react";
import { View } from "react-native";

export const ContainerSection = ({ style, children }) => {
  return (
    <View
      style={[
        {
          marginVertical: 15,
          marginHorizontal: 15
        },
        style
      ]}
    >
      {children}
    </View>
  );
};
