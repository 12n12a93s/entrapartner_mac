import React, { Component } from "react";
import { onPress, Image } from "react-native";
import { Container, ContainerSection, Button, Title } from "./components";
import { connect } from "react-redux";

class DataPersonal extends Component {
  render() {
    const { headerContainer, headerLogo, blackColor } = styles;
    return (
      <Container>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/Verifikasi.png")}
            style={headerLogo}
          />
        </ContainerSection>

        <ContainerSection>
          <Title iscenter style={blackColor}>
            Sedikit Lagi.
          </Title>
        </ContainerSection>

        <ContainerSection>
          <Title iscenter>bla bla bla bla bla bla bla </Title>
          <Title iscenter>bla bla bla bla bla bla bla </Title>
        </ContainerSection>

        <ContainerSection>
          <Button title={"Press Login Login Bla"} />
        </ContainerSection>

        <ContainerSection>
          <Title iscenter>ganti bla</Title>
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(DataPersonal);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 18
  }
};
