import React, { Component } from "react";
import { onPress, Image } from "react-native";
import {
    Container,
    ContainerSection,
    Button,
    Title,
    ArrowBack
} from "./components";
import { connect } from "react-redux";

class BisnisHome extends Component {
    render() {
        const { headerContainer, headerLogo, right, rowTop, posNanti } = styles;
        return (
            <Container>
                <ContainerSection style={rowTop}>
                    <ArrowBack
                        imgSrc={require("../assets/arrow.png")} />
                    <Title style={right}>Skip</Title>
                </ContainerSection>
                <ContainerSection style={headerContainer}>
                    <Image
                        source={require("../assets/bisnishome.png")}
                        style={headerLogo}
                    />
                </ContainerSection>
                <ContainerSection style={headerContainer}>
                    <Title isheadline3>Pilih Bisnis{'\n'}</Title>
                    <Title iscenter isdescription >Untuk memulai, tentukan dahulu{'\n'}
                        bisnis pariwisata yang akan kamu{'\n'}
                        bangun</Title>
                </ContainerSection>
                <ContainerSection>
                    <Button title={"Pilih Bisnisku"} />
                    <Title iscenter style={posNanti}>Nanti Saja</Title>
                </ContainerSection>
            </Container>
        )
    }
}
const mapStateProps = state => {
    return {};
};
export default connect(mapStateProps, {})(BisnisHome);

const styles = {
    headerContainer: {
        justifyContent: "center",
        alignItems: "center"
    },
    headerLogo: {
        width: 300,
        height: 300,
        resizeMode: "contain",
    },
    right: {
        paddingLeft: 320,
        marginTop: 20,
        fontSize: 14,
        color: "#828282",
        position: "absolute"
    },
    rowTop: {
        flexDirection: 'row',
        position: "relative"
    },
    posNanti: {
        marginTop: 10
    }
}