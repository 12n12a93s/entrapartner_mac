import React, { Component } from "react";
import { onPress, Image } from "react-native";
import { Container, ContainerSection, Button, Title, ArrowBack } from "./components";
import { connect } from "react-redux";

class Verifikasi extends Component {
  render() {
    const { headerContainer, headerLogo, blackColor, smallText } = styles;
    return (
      <Container>
        <ContainerSection>
          <ArrowBack
            imgSrc={require("../assets/arrow.png")} />
        </ContainerSection>
        <ContainerSection style={headerContainer}>
          <Image
            source={require("../assets/Verifikasi.png")}
            style={headerLogo}
          />
        </ContainerSection>

        <ContainerSection>
          <Title iscenter style={blackColor}>
            Email Terkirim
          </Title>
        </ContainerSection>

        <ContainerSection>
          <Title iscenter style={smallText}>Silahkan buka email anda dan ikuti </Title>
          <Title iscenter style={smallText}>langkah yang ada</Title>
        </ContainerSection>

        <ContainerSection>
          <Button title={"Login Kembali"} />
        </ContainerSection>
      </Container>
    );
  }
}

const mapStateProps = state => {
  return {};
};
export default connect(mapStateProps, {})(Verifikasi);

const styles = {
  headerContainer: {
    marginTop: 80,
    marginBottom: 40,
    justifyContent: "center",
    alignItems: "center"
  },
  headerLogo: {
    width: 200,
    resizeMode: "contain"
  },
  blackColor: {
    fontFamily: "Monterchi-Serif-Extrabold-trial",
    fontSize: 18,
    color: "#828282"
  },
  smallText: {
    color: "#828282",
    fontSize: 16
  }
};
