import { createStackNavigator } from "react-navigation-stack";
import { createAppContainer } from "react-navigation";
import Drawer from "./Drawer";
import Login from "../screen/Login";
import Register from "../screen/Register";
import Verifikasi from "../screen/Verifikasi";
import DataPersonal from "../screen/DataPersonal";
import ResetPass from "../screen/ResetPass";
import Welcome from "../screen/Welcome";
import BisnisHome from "../screen/BisnisHome";
import ChooseBisnis from "../screen/ChooseBisnis";

export default createAppContainer(
  createStackNavigator(
    {
      ChooseBisnis,
      Drawer
    },
    {
      defaultNavigationOptions: {
        header: null
      }
    }
  )
);
