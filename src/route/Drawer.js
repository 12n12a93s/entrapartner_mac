import { createDrawerNavigator } from "react-navigation-drawer";
import Login from "../screen/Login";
import Register from "../screen/Register";
import Verifikasi from "../screen/Verifikasi";
import ResetPass from "../screen/ResetPass";
import DataPersonal from "../screen/DataPersonal";
import Welcome from "../screen/Welcome";
import BisnisHome from "../screen/BisnisHome";
import ChooseBisnis from "../screen/ChooseBisnis";

export default createDrawerNavigator({
  Login,
  Register,
  Verifikasi,
  ResetPass,
  DataPersonal,
  Welcome,
  BisnisHome,
  ChooseBisnis
});
