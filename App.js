import React, { Component } from "react";
import { Root } from "native-base";
import { createStore, applyMiddleware } from "redux";
import { Provider } from "react-redux";
import ReduxThunk from "redux-thunk";
import NavigationService from "./src/service/NavigationService";
import reducers from "./src/service/reducer";
import Route from "./src/route";
import Axios from "axios";
// import Splash from "react-native-splash-screen";
const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));

class App extends Component {
  componentDidMount() {
    // Splash.hide();
    Axios.defaults.baseURL = "https://triyo.org/sikd/api/";
    // Axios.defaults.headers.common['Authorization'] = AUTH_TOKEN
    Axios.defaults.headers.post["Content-Type"] = "multipart/form-data";
  }

  render() {
    return (
      <Root>
        <Provider store={store}>
          <Route
            ref={navigatorRef =>
              NavigationService.setTopLevelNavigator(navigatorRef)
            }
          />
        </Provider>
      </Root>
    );
  }
}

export default App;
